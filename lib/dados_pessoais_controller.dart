import 'package:mobx/mobx.dart';
part 'dados_pessoais_controller.g.dart';

class DadosPessoaisController = _DadosPessoaisController with _$DadosPessoaisController;

abstract class _DadosPessoaisController with Store {

  List dadosPessoais = [
    {"label": "Foto do irmao", "conteudo": "lib/assets/back.jpeg", "tipo": "foto"},
    {"label": "Meu nome", "conteudo": "Eugênio Mazzini"},
    {"label": "Nascimento", "conteudo": "14/09/1966"},
    {"label": "CPF", "conteudo": "203.318.002-68"},
    {"label": "RG", "conteudo": "232008"},
    {"label": "Orgão expeditor", "conteudo": "SSP"},
    {"label": "País de nascimento", "conteudo": "Brasil"},
    {"label": "UF de nascimento", "conteudo": "PA"},
    {"label": "Cidade de nascimento", "conteudo": "Belém"},
    {"label": "Grau de instrução", "conteudo": "Superior Completo"},
    {"label": "Formação", "conteudo": "Matemática"},
    {"label": "Profissão", "conteudo": "Empresário"},
    {"label": "Ocupação", "conteudo": "Administrador"},
    {"label": "Local de trabalho", "conteudo": "empyt"},
    {"label": "Especialidade", "conteudo": "empty"},
    {"label": "É Diabético", "conteudo": "não"},
    {"label": "É Doador de orgãos ?", "conteudo": "empty"},
    {"label": "É Doador de sangue ?", "conteudo": "empty"},
    {"label": "É Doador de medula ?", "conteudo": "não"},
    {"label": "Tipo sanguíneo", "conteudo": "empty"},
    {"label": "Email", "conteudo": "mazzini@yahoo.com.br"},
    {"label": "Email financeiro", "conteudo": "empty"},
    {"label": "Site pessoal", "conteudo": "empty"},
    {"label": "Telefone residencial", "conteudo": "(65)3028-4699"},
    {"label": "Telefone comercial", "conteudo": "(65)3028-4045"},
    {"label": "Telefone celular", "conteudo": "(65)98463-1380"},
    {"label": "Religião", "conteudo": "Católica"},
    {"label": "Estado civil", "conteudo": "Casado"},
    {"label": "Casamento", "conteudo": "01/02/2001"},
    {"label": "Compartilhar dados profissionais", "conteudo": "Sim"},
    {"label": "Nome da mãe", "conteudo": "Maria Odede da Silva Mazzini"},
    {"label": "Nome do pai", "conteudo": "Antônil Z Cardoso de Souza"},
  ];

}