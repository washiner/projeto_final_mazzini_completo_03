import 'package:aula_app_mazzini_02/lista_dados_pessoais.dart';
import 'package:flutter/material.dart';

class MeusDados extends StatefulWidget {
  @override
  _MeusDadosState createState() => _MeusDadosState();
}

class _MeusDadosState extends State<MeusDados> with SingleTickerProviderStateMixin{

  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 6, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Meus Dados"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.white70,
            child: TabBar(
              indicatorColor: Colors.red,
              indicatorWeight: 6,
              isScrollable: true,
              controller: _tabController,
              tabs: [
                Tab(child: Text(("Pessoal"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7)),),),
                Tab(child: Text(("Familiares"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7))),),
                Tab(child: Text(("Profissional"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7))),),
                Tab(child: Text(("Endereço"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7))),),
                Tab(child: Text(("Declaração de vontade"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7))),),
                Tab(child: Text(("Evolução"), style: TextStyle(fontSize: 16, color: Colors.black.withOpacity(0.7))),)
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
                children: [
                  ListaDadosPessoais(),
                  Tab(child: Container(color: Colors.yellow,),),
                  Tab(child: Container(color: Colors.red,),),
                  Tab(child: Container(color: Colors.blue,),),
                  Tab(child: Container(color: Colors.purple,),),
                  Tab(child: Container(color: Colors.blueGrey,),),
                ]
            ),
          )
        ],
      ),
    );
  }
}
