import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'dados_pessoais_controller.dart';

class Nascimento extends StatefulWidget {
  final String label;
  final String conteudo;
  final int index;
  final DadosPessoaisController controller;

  const Nascimento({Key key, @required this.label, @required this.controller, @required this.conteudo, @required this.index}) : super(key: key);
  @override
  _NascimentoState createState() => _NascimentoState();
}

class _NascimentoState extends State<Nascimento> {

  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    _textEditingController.text = widget.conteudo;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.label),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: TextFormField(
              controller: _textEditingController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(18)
                ),
                labelText: widget.label,
                labelStyle: TextStyle(fontSize: 18)
              ),
            ),
          ),
          SizedBox(height: 140),
          RaisedButton(
            color: Colors.blue,
            child: Text("Proximo", style: TextStyle(color: Colors.white),),
              onPressed: (){

              widget.controller.dadosPessoais[widget.index]["conteudo"] == _textEditingController.value.text ? print(" correto ta igual ") : print(" sinto muito e diferente");

              //um jeito de busca o dado da lista mais posso usar ternario ou fechando com chave pra usar else
                // if(widget.controller.dadosPessoais[widget.index]["conteudo"] == _textEditingController.value.text)
                //   print("E igual ");
                // if(widget.controller.dadosPessoais[widget.index]["conteudo"] != _textEditingController.value.text)
                //   print(" e diferente");

                widget.controller.dadosPessoais[widget.index]["conteudo"] = _textEditingController.value.text;
              }
          )
        ],
      ),
    );
  }
}
