import 'package:flutter/material.dart';

class MeuNome extends StatefulWidget {
  final String label;
  final String conteudo;

  const MeuNome({Key key, @required this.label, @required this.conteudo}) : super(key: key);
  @override
  _MeuNomeState createState() => _MeuNomeState();
}

class _MeuNomeState extends State<MeuNome> {

  TextEditingController  _textEditingController = TextEditingController();
  @override
  void initState() {
   _textEditingController.text = widget.conteudo;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.label),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: TextFormField(
              controller: _textEditingController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20)
                ),
                labelText: widget.label,
                  labelStyle: TextStyle(fontSize: 18)
              ),
            ),
          ),
          SizedBox(height: 140),
          RaisedButton(
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16)
            ),
            child: Text("Proximo", style: TextStyle(color: Colors.white),),
              onPressed: (){}
          )
        ],
      ),

    );
  }
}
