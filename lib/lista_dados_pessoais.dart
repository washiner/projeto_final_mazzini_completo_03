import 'package:aula_app_mazzini_02/casamento.dart';
import 'package:aula_app_mazzini_02/cidade_nascimento.dart';
import 'package:aula_app_mazzini_02/compartilhar_dados_profissional.dart';
import 'package:aula_app_mazzini_02/cpf.dart';
import 'package:aula_app_mazzini_02/dados_pessoais_controller.dart';
import 'package:aula_app_mazzini_02/e_diabetico.dart';
import 'package:aula_app_mazzini_02/e_doador_medula.dart';
import 'package:aula_app_mazzini_02/e_doador_orgao.dart';
import 'package:aula_app_mazzini_02/e_doador_sangue.dart';
import 'package:aula_app_mazzini_02/editar_dados.dart';
import 'package:aula_app_mazzini_02/email.dart';
import 'package:aula_app_mazzini_02/email_financeiro.dart';
import 'package:aula_app_mazzini_02/especialidade.dart';
import 'package:aula_app_mazzini_02/estado_civil.dart';
import 'package:aula_app_mazzini_02/formacao.dart';
import 'package:aula_app_mazzini_02/foto.dart';
import 'package:aula_app_mazzini_02/grau_instrucao.dart';
import 'package:aula_app_mazzini_02/local_trabalho.dart';
import 'package:aula_app_mazzini_02/meu_nome.dart';
import 'package:aula_app_mazzini_02/nascimento.dart';
import 'package:aula_app_mazzini_02/nome_mae.dart';
import 'package:aula_app_mazzini_02/nome_pai.dart';
import 'package:aula_app_mazzini_02/ocupacao.dart';
import 'package:aula_app_mazzini_02/orgao_expeditor.dart';
import 'package:aula_app_mazzini_02/pais_de_nascimento.dart';
import 'package:aula_app_mazzini_02/profissao.dart';
import 'package:aula_app_mazzini_02/religiao.dart';
import 'package:aula_app_mazzini_02/rg.dart';
import 'package:aula_app_mazzini_02/site_pessoal.dart';
import 'package:aula_app_mazzini_02/telefone_celular.dart';
import 'package:aula_app_mazzini_02/telefone_comercial.dart';
import 'package:aula_app_mazzini_02/telefone_residencial.dart';
import 'package:aula_app_mazzini_02/tipo_sanguineo.dart';
import 'package:aula_app_mazzini_02/uf_de_nascimento.dart';
import 'package:flutter/material.dart';

class ListaDadosPessoais extends StatefulWidget {
  @override
  _ListaDadosPessoaisState createState() => _ListaDadosPessoaisState();
}

class _ListaDadosPessoaisState extends State<ListaDadosPessoais> {

   final controller = DadosPessoaisController();

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: controller.dadosPessoais.length,
      itemBuilder:
        (BuildContext context, int index) {

        String tipo = controller.dadosPessoais[index]["tipo"];
        String label = controller.dadosPessoais[index]["label"];
        String conteudo = controller.dadosPessoais[index]["conteudo"];

        if(tipo == "foto")
          return itemFoto(conteudo);
        return itemList(label, conteudo, index);
        },
    );
  }

  Widget itemFoto(String conteudo) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (_) => Foto()));
            },
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage(conteudo)
                  ),
                  color: Colors.blue
              ),
              height: 150,
            ),

            // CircleAvatar(
            //             backgroundImage: AssetImage("lib/assets/back.jpeg"),
            //             radius: 100.0,
            //
            //           ),

          ),
        ),
        Divider(
          indent: 14,
          endIndent: 14,
          thickness: 2,
        )
      ],
    );
  }
  Widget itemList(String label, String conteudo, int index) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, left: 14, right: 14),
          child: Row(
            children: [
              Text(label, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black.withOpacity(0.7)),),
              Expanded(
                child: GestureDetector(
                  onTap: (){
                    if(controller.dadosPessoais[index]["label"] == "Meu nome")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => MeuNome(label: label, conteudo: conteudo,)));
                    if(controller.dadosPessoais[index]["label"] == "Nascimento")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Nascimento(label: label, controller: controller, conteudo: conteudo, index: index,)));
                    if(controller.dadosPessoais[index]["label"] == "CPF")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => CPF()));
                    if(controller.dadosPessoais[index]["label"] == "RG")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => RG()));
                    if(controller.dadosPessoais[index]["label"] == "Orgão expeditor")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => OrgaoExpeditor()));
                    if(controller.dadosPessoais[index]["label"] == "País de nascimento")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => PaisDeNascimento()));
                    if(controller.dadosPessoais[index]["label"] == "UF de nascimento")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => UFNascimento()));
                    if(controller.dadosPessoais[index]["label"] == "Cidade de nascimento")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => CidadeNascimento()));
                    if(controller.dadosPessoais[index]["label"] == "Grau de instrução")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => GrauInstrucao()));
                    if(controller.dadosPessoais[index]["label"] == "Formação")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Formacao()));
                    if(controller.dadosPessoais[index]["label"] == "Profissão")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Profissao()));
                    if(controller.dadosPessoais[index]["label"] == "Ocupação")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Ocupacao()));
                    if(controller.dadosPessoais[index]["label"] == "Local de trabalho")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => LocalTrabalho()));
                    if(controller.dadosPessoais[index]["label"] == "Especialidade")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Especialidade()));
                    if(controller.dadosPessoais[index]["label"] == "É Diabético")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EDiabetico()));
                    if(controller.dadosPessoais[index]["label"] == "É Doador de orgãos ?")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EDoadorOrgao()));
                    if(controller.dadosPessoais[index]["label"] == "É Doador de sangue ?")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EDoadorSangue()));
                    if(controller.dadosPessoais[index]["label"] == "É Doador de medula ?")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EDoadorMedula()));
                    if(controller.dadosPessoais[index]["label"] == "Tipo sanguíneo")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => TipoSanguineo()));
                    if(controller.dadosPessoais[index]["label"] == "Email")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Email()));
                    if(controller.dadosPessoais[index]["label"] == "Email financeiro")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EmailFinanceiro()));
                    if(controller.dadosPessoais[index]["label"] == "Site pessoal")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => SitePessoal()));
                    if(controller.dadosPessoais[index]["label"] == "Telefone residencial")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => TelefoneResidencial()));
                    if(controller.dadosPessoais[index]["label"] == "Telefone comercial")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => TelefoneComercial()));
                    if(controller.dadosPessoais[index]["label"] == "Telefone celular")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => TelefoneCelular()));
                    if(controller.dadosPessoais[index]["label"] == "Religião")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Religiao()));
                    if(controller.dadosPessoais[index]["label"] == "Estado civil")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => EstadoCivil()));
                    if(controller.dadosPessoais[index]["label"] == "Casamento")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => Casamento()));
                    if(controller.dadosPessoais[index]["label"] == "Compartilhar dados profissionais")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => CompartilharDadosProfissionais()));
                    if(controller.dadosPessoais[index]["label"] == "Nome da mãe")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => NomeMae()));
                    if(controller.dadosPessoais[index]["label"] == "Nome do pai")
                      Navigator.push(context, MaterialPageRoute(builder: (_) => NomePai()));


                    //tela editar dados
                    //Navigator.push(context, MaterialPageRoute(builder: (_) => EditarDados(dadosPessoais: controller.dadosPessoais, index: index,)));
                  },
                  child: Container(
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(conteudo, style: TextStyle(fontSize: 15, color: Colors.black.withOpacity(0.6)),),
                        Icon(Icons.arrow_forward_ios, size: 14, color: Colors.black.withOpacity(0.7),),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Divider(
          height: 5,
          thickness: 2,
          indent: 14,
          endIndent: 14,
        )
      ],
    );
  }

}
