import 'package:flutter/material.dart';

class EditarDados extends StatefulWidget {
  final List dadosPessoais;
  final int index;

  const EditarDados({Key key, @required this.dadosPessoais, @required this.index}) : super(key: key);
  @override
  _EditarDadosState createState() => _EditarDadosState();
}

class _EditarDadosState extends State<EditarDados> {

  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    _textEditingController.text = widget.dadosPessoais[widget.index]["conteudo"];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.dadosPessoais[widget.index]["label"]),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: TextFormField(
              controller:_textEditingController,
              decoration: InputDecoration(
                labelText: widget.dadosPessoais[widget.index]["label"],
                labelStyle: TextStyle(fontSize: 20),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20)
                )
              ),
            ),
          ),
          SizedBox(height: 150),
          RaisedButton(
            color: Colors.blue,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(14)

            ),
            child: Text("Proximo", style: TextStyle(fontSize: 16, color: Colors.white),),
              onPressed: (){
              widget.dadosPessoais[widget.index]["conteudo"] == _textEditingController.text ? print("E igual") : print("E diferente");
              widget.dadosPessoais[widget.index]["conteudo"] = _textEditingController.value.text;
               print("A Lista Foi Atualizada com Sucesso");
              }
          )
        ],
      ),
    );
  }
}
